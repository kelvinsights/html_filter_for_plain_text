<?php
/**
 * @file
 * html_filter_for_plain_text.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function html_filter_for_plain_text_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'html_filter_for_plain_text';
  $formatter->label = 'HTML filter for plain text';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'text_long';
  $formatter->code = 'print ($variables[\'#items\'][0][\'value\']);';
  $formatter->fapi = '';
  $export['html_filter_for_plain_text'] = $formatter;

  return $export;
}
