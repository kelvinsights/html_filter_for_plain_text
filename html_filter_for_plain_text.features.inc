<?php
/**
 * @file
 * html_filter_for_plain_text.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function html_filter_for_plain_text_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
}
